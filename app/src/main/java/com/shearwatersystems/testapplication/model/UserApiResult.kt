package com.shearwatersystems.testapplication.model


import com.fasterxml.jackson.annotation.JsonProperty

data class UserApiResult(
        @JsonProperty("data")
        val `data`: UserDetail?
) {
    data class UserDetail(
            @JsonProperty("color")
            val color: String?, // #98B2D1
            @JsonProperty("id")
            val id: Int?, // 1
            @JsonProperty("name")
            val name: String?, // cerulean
            @JsonProperty("pantone_value")
            val pantoneValue: String?, // 15-4020
            @JsonProperty("year")
            val year: Int? // 2000
    )
}