package com.shearwatersystems.testapplication.model

import com.fasterxml.jackson.annotation.JsonProperty

data class UserData(
    @JsonProperty("avatar")
    val avatar: String?, // https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg
    @JsonProperty("email")
    val email: String?, // tracey.ramos@reqres.in
    @JsonProperty("first_name")
    val firstName: String, // Tracey
    @JsonProperty("id")
    val id: Int, // 6
    @JsonProperty("last_name")
    val lastName: String // Ramos
)
