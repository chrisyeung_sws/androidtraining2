package com.shearwatersystems.testapplication.model


import com.fasterxml.jackson.annotation.JsonProperty

data class UsersApiResult(
    @JsonProperty("data")
    val `data`: List<UserData>?,
    @JsonProperty("page")
    val page: Int, // 2
    @JsonProperty("per_page")
    val perPage: Int, // 3
    @JsonProperty("total")
    val total: Int, // 12
    @JsonProperty("total_pages")
    val totalPages: Int // 4
)