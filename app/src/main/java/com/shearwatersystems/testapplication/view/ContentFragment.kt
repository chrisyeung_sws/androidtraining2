package com.shearwatersystems.testapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.shearwatersystems.testapplication.R
import com.shearwatersystems.testapplication.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.content_fragment.*

class ContentFragment : Fragment() {
    private val viewModel by lazy { ViewModelProviders.of(activity!!).get(UserViewModel::class.java) }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.content_fragment, container, false)
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        viewModel.selectedUser.observe(this, Observer {
            if (it != null) {
                etName.setText(it.name)
                etYear.setText(it.year.toString())
            }
        })
    }
}
