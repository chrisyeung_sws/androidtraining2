package com.shearwatersystems.testapplication.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.shearwatersystems.testapplication.R
import com.shearwatersystems.testapplication.viewmodel.UserViewModel

class MainActivity : AppCompatActivity() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(UserViewModel::class.java) }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        viewModel.selectedUser.observe(this, Observer {
            if (it != null) {
                supportFragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.flContent, ContentFragment())
                        .commit()
            }
        })
        
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount == 0) {
                viewModel.selectedUser.value = null
            }
        }
    }
}
