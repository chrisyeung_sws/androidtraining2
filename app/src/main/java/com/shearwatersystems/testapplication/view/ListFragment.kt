package com.shearwatersystems.testapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shearwatersystems.testapplication.R
import com.shearwatersystems.testapplication.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.list_fragment.*
import kotlinx.android.synthetic.main.list_item.view.*

class ListFragment : Fragment() {
    private val viewModel by lazy { ViewModelProviders.of(activity!!).get(UserViewModel::class.java) }
    private val userAdapter by lazy { UserAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.list_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvUser.layoutManager = LinearLayoutManager(context)
        rvUser.adapter = userAdapter

        // call viewmodel to load data
        viewModel.loadUsers()
        viewModel.user.observe(this, Observer { userDataList ->
            userAdapter.setData(userDataList.map { User(it.firstName, it.lastName, it.id) })
        })
    }

    private fun onUserSelected(user: User) {
        // call viewmodel to load user
        viewModel.selectUser(user.id)
    }

    data class User(
        val firstName: String,
        val lastName: String,
        val id: Int
    )

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var user: User? = null

        init {
            itemView.llBase.setOnClickListener {
                user?.let { onUserSelected(it) }
            }
        }

        fun bindData(user: User) {
            this.user = user

            itemView.tvFirstName.text = user.firstName
            itemView.tvLastName.text = user.lastName
        }
    }

    inner class UserAdapter : RecyclerView.Adapter<UserViewHolder>() {
        private val data = ArrayList<User>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder =
            UserViewHolder(layoutInflater.inflate(R.layout.list_item, parent, false))

        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
            holder.bindData(data[position])
        }

        fun setData(users: Collection<User>) {
            data.clear()
            data.addAll(users)
            notifyDataSetChanged()
        }
    }
}
