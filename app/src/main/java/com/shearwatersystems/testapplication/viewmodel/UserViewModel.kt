package com.shearwatersystems.testapplication.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shearwatersystems.testapplication.api.ApiHelper
import com.shearwatersystems.testapplication.model.UserApiResult
import com.shearwatersystems.testapplication.model.UserData
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class UserViewModel : ViewModel(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO
    
    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
    
    /** result of [loadUsers] */
    val user = MutableLiveData<List<UserData>>()
    
    /** result of [selectedUser] */
    val selectedUser = MutableLiveData<UserApiResult.UserDetail?>()
    
    private val apiHelper = ApiHelper()
    
    fun loadUsers() {
        launch {
            try {
                val result = apiHelper.userServices.getUsers(1).await()
                withContext(Dispatchers.Main) {
                    user.value = result.data
                }
            } catch (e: Exception) {
                //TODO handle API error
                e.printStackTrace()
            }
        }
    }
    
    fun selectUser(id: Int) {
        launch {
            try {
                val result = apiHelper.userServices.getUser(id).await()
                withContext(Dispatchers.Main) {
                    selectedUser.value = result.data
                }
            } catch (e: Exception) {
                //TODO handle API error
                e.printStackTrace()
            }
        }
    }
}