package com.shearwatersystems.testapplication.api

import com.shearwatersystems.testapplication.model.UserApiResult
import com.shearwatersystems.testapplication.model.UsersApiResult
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserServices {
    @GET("/api/users")
    fun getUsers(@Query("page") page: Int): Deferred<UsersApiResult>

    @GET("/api/user/{userId}")
    fun getUser(@Path("userId") userId: Int): Deferred<UserApiResult>
}